# Location data from Elasticsearch in Grafana dashboard

## Used software

- [Elasticsearch 6.2.4](https://www.elastic.co/de/downloads/elasticsearch) --> [http://localhost:9200](http://localhost:9200)
- [Kibana 6.2.4](https://www.elastic.co/de/downloads/kibana) --> [http://localhost:5601](http://localhost:5601)
- [Logstash 6.2.4](https://www.elastic.co/de/downloads/logstash)
- [Grafana 5.1.3](https://grafana.com/get) --> [http://localhost:3000](http://localhost:3000)
- [Worlmap panel v0.1.0](https://grafana.com/plugins/grafana-worldmap-panel)
- [Go 1.10.2 (for mass testdata)](https://golang.org/)

## Create index in elasticsearch

At first we have to create an index in elasticsearch where we want to store our data.  
Enter the following code in the [Console of Kibana Dev Tools](http://localhost:5601/app/kibana#/dev_tools/console?_g=())

```http
PUT /buginsight
{
  "settings": {
    "index": {
      "number_of_replicas": "1",
      "number_of_shards": "5"
    }
  },
  "mappings": {
    "bugReport": {
      "properties": {
        "title": {
          "type": "keyword",
          "fields": {
            "analyzed": {
              "type": "text"
            }
          }
        },
        "state": {
          "type": "keyword",
          "fields": {
            "analyzed": {
              "type": "text"
            }
          }
        },
        "description": {
          "type": "keyword",
          "fields": {
            "analyzed": {
              "type": "text"
            }
          }
        },
        "city": {
          "type": "keyword",
          "fields": {
            "analyzed": {
              "type": "text"
            }
          }
        },
        "location": {
          "type": "geo_point"
        },
        "job_size": {
          "type": "keyword",
          "fields": {
            "analyzed": {
              "type": "integer"
            }
          }
        },
        "creation_time": {
          "type": "date",
          "format": "strict_date_optional_time||epoch_millis"
        }
      }
    }
  }
}
```

## Put some testdata in our created index buginsight

Open the file ```testData\testdata.data```, copy the content to the Kibana Console, mark and execute the statements

## Verify your data

Let's check if your data is in the elasticsearch index by searching for it.  
Execute the following query in the Kibana Console:

```http
GET buginsight/_search?size=20
{
  "query": {
    "match_all": {}
  }
}
```

You should see a result similar to the following:

```json
{
  "took": 4,
  "timed_out": false,
  "_shards": {
    "total": 5,
    "successful": 5,
    "skipped": 0,
    "failed": 0
  },
  "hits": {
    "total": 10,
    "max_score": 1,
    "hits": [
      {
        "_index": "buginsight",
        "_type": "bugReport",
        "_id": "YabQMmQBjq2FZmg-QkNU",
        "_score": 1,
        "_source": {
          "title": "dino database crashed",
          "state": "NEW",
          "location": "48.137154, 11.576124",
          "description": "after power outage our antique database won't start anymore",
          "job_size": 20,
          "creation_time": "2018-06-22T07:21:14.834Z"
        }
      },
      ...
```

## Visualize the data within Grafana

1. Install Grafana and the Worldmap Panel plugin.
2. Create a new Elasticseach Datasource in Grafana like in the following picture  

![elastic datasource in grafana](pictures/elastic_datasource_grafana.png)
3. Import the ```BugOverview-Grafana-Dashboard.json```  
4. In the map of the new dashbaord you can see the geolocation of our test data we put into elasticsearch

## Insert more data

If you want to add more test data to elasticsearch you can use the simple go script ```testData/createTestData.go```

```shell
go run createTestData.go
```

After executing this script your map could look like in the following picture:
![geolocation in grafana](grafana/Geolocation_dashboard.png)