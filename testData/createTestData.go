package main

import (
	"fmt"
	"io/ioutil"
	"math/rand"
	"net/http"
	"strconv"
	"strings"
	"time"
)

var locationMap = make(map[string]string)
var locations = make([]string, 0, len(locationMap))
var jobsize [8]int
var payload string

func initLocations() {
	//locationMap = make(map[string]string)
	locationMap = map[string]string{
		"Munich":    "48.137154, 11.576124",
		"Berlin":    "52.520008, 13.404954",
		"Cologne":   "50.935173, 6.953101",
		"Karlsruhe": "49.006889, 8.403653",
		"Hamburg":   "53.551086, 9.993682",
		"Atlanta":   "33.753746, -84.386330",
		"Zug":       "47.166168, 8.515495",
	}
	//locations := make([]string, 0, len(locationMap))
	for k := range locationMap {
		locations = append(locations, k)
	}
	//locations[0] = "48.137154, 11.576124"
	//locations[1] = "52.520008, 13.404954"
	//locations[2] = "50.935173, 6.953101"
	//locations[3] = "49.006889, 8.403653"
	//locations[4] = "53.551086, 9.993682"
	//locations[5] = "33.753746, -84.386330"
	//locations[6] = "47.166168, 8.515495"
}

func initJobsizes() {
	jobsize[0] = 1
	jobsize[1] = 2
	jobsize[2] = 5
	jobsize[3] = 8
	jobsize[4] = 13
	jobsize[5] = 20
	jobsize[6] = 40
	jobsize[7] = 100
}

func initValues() {
	initLocations()
	initJobsizes()
	payload = `{
		"title": "error §ID§ found",
		"state": "NEW",
		"location": "§LOC§",
		"description": "Lorem ipsum dolor sit amet",
		"job_size": §JOBSIZE§,
		"creation_time": "§CTIME§",
		"city": "§CITY§"
	   }`
}

func main() {
	initValues()
	s := rand.NewSource(time.Now().Unix())
	r := rand.New(s) // initialize local pseudorandom generator

	for i := 0; i < 1000; i++ {
		t := time.Now()
		randomLoc := locations[r.Intn(len(locations))]
		randomSize := jobsize[r.Intn(len(jobsize))]
		randomGeoLoc := locationMap[randomLoc]
		result := strings.Replace(payload, "§ID§", strconv.Itoa(i), -1)
		result = strings.Replace(result, "§LOC§", randomGeoLoc, -1)
		result = strings.Replace(result, "§JOBSIZE§", strconv.Itoa(randomSize), -1)
		result = strings.Replace(result, "§CTIME§", t.Format(time.RFC3339), -1)
		result = strings.Replace(result, "§CITY§", randomLoc, -1)
		fmt.Println(result)
		req, err := http.NewRequest("POST", "http://localhost:9200/buginsight/bugReport", strings.NewReader(result))
		req.Header.Set("Content-Type", "application/json")

		client := &http.Client{}
		resp, err := client.Do(req)
		if err == nil {
			defer resp.Body.Close()

			fmt.Println("response Status:", resp.Status)
			fmt.Println("response Headers:", resp.Header)
			body, _ := ioutil.ReadAll(resp.Body)
			fmt.Println("response Body:", string(body))
		} else {

			fmt.Println(err)
		}
		//time.Sleep(1 * time.Second)
	}

}
